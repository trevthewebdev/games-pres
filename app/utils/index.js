exports.proxy = function proxy(fn, context) {
  if (typeof fn !== 'function') {
    throw "Error: Proxy param is not a function! " + arguments.callee.caller.toString()
  }

  var args = [].slice.call(arguments, 2)
  var proxyFn = function proxyFn() {
    return fn.apply(context, args.concat([].slice.call(arguments)))
  }

  return proxyFn;
}


/**
 * Authorization header
 *
 * takes the current express request, and the superagent request and adds the
 * cookie that was set to the outgoing api request
 * @param req (express request)
 * @param request (superagent request)
 */
exports.authHead = function authHead(req, request) {
  var cookie = req.cookies.games;

  if (cookie) {
    request.header['Authorization'] = cookie;
  }

  return request;
};
