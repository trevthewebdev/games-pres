import express from 'express'
import routes from './routes'
import middleware from './middleware';
const app = express()

// Allows serving of static files from public
app.use(express.static('public'))

// setup the app middleware
middleware(app);

// Include Routes
app.use(routes)

// Defines out default view path
app.set('views', __dirname + '/views')
app.set('x-powered-by', false)
app.set('view engine', 'pug')

export default app;
