import React, { Component } from 'react';
import {render} from 'react-dom';

import Comp from './comp';

class App extends Component {
  render() {
    return(
      <div className="wrapper">
        <h2>We are from games, we know what you did oh man</h2>
        <Comp />
      </div>
    );
  }
}

window.onload = () => {
  render(<App />, document.getElementById('app'));
}
