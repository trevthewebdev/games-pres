var Agent = require('superagent')
var router = require('express').Router()
// var controller = require('./public_controller')

router.get('/', function (req, res, next) {
  res.redirect('dashboard/')
})

router.get('/login', function (req, res, next) {
  if (req.query.status === '401') {
    return res.render('public/login', { error: {
      message: 'Unauthorized, please log in'
    }});
  }

  res.render('public/login')
})

router.post('/login', function (req, res, next) {
  Agent
    .post('http://localhost:3001/api/players/authenticate')
    .send(req.body)
    .end(function (err, result) {
      if (err) return res.render('public/login', {
        error: err.response.res.body
      })
      // console.log(result.body);
      res.append('authenticated', result.body.token);
      res.redirect('/dashboard?token=' + result.body.token);
    })
})

module.exports = router
