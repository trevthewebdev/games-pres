var Agent = require('superagent')
var util = require('../../utils')

exports.home = function (req, res, next) {
  var cookie = req.cookies.games
  var token = req.query.token || cookie

  if (!cookie || cookie !== token) {
    res.cookie('games', token)
  }

  Agent
    .get('http://localhost:3001/api/games')
    .use(util.proxy(util.authHead, null, req))
    .end(function (err, result) {
      if (err && err.response.statusCode === 401) {
        return res.redirect('/login?status=401')
      }

      if (err) return next(err)
      res.render('dashboard/index', { games: result.body })
    })
}


/*

  1. Check to see if there is a cookie set
  2. Check to see if that cookie has a different token
    - If so, drop the cookie and replace it
    - else, do nothing
  3. Setup middleware that reads cookie
  4. Setup middleware that responds if the user is not authorized to access something

*/
