var router = require('express').Router()
var controller = require('./players_controller')

// Players
router.get('/', controller.players)

module.exports = router;
