import  express from 'express'
import controller from './dashboard_controller'
import getUser from '../../middleware/get_user'
const router = express.Router();

router.get('/', controller.home)

// Check if the user is authenticated before running any other route
router.use(getUser)

router.use('/games', require('./games'))
router.use('/events', require('./events'))
router.use('/players', require('./players'))

export default router
