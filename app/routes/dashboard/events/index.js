var router = require('express').Router()
var controller = require('./events_controller')

// Events
router.get('/', controller.events)

module.exports = router;
