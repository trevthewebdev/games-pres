var router = require('express').Router()
var controller = require('./games_controller')

// GETs
router.get('/', controller.games)

module.exports = router;
