import express from 'express'
import dashboard from './dashboard'
import pub from './public'
const router = express.Router()

router.use('/dashboard', dashboard)
router.use('/', pub)

router.use('*', (req, res) => {
  res.render('errors/404')
})

export default router
