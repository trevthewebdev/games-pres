import Agent from 'superagent'
import util from '../utils'

export default (req, res, next) => {
  Agent
    .get('http://localhost:3001/api/players')
    .use(util.proxy(util.authHead, null, req))
    .end((err, result) => {
      // console.log("ERROR", err);
      // console.log("RESULT", result.res.body);
      if (err && err.response.statusCode === 401) {
        return res.redirect('/login?status=401')
      }
      next()
    })
}
