import cookieParser from 'cookie-parser'
import errors from './errors'
import bodyParser from 'body-parser'
import { useWebpackMiddleware } from './webpack.js'

export default (app) => {
  app.use(bodyParser.urlencoded({ extend: true }))
  app.use(bodyParser.json())
  app.use(cookieParser())

  // @TODO this needs to be cleaned up and production version needs to actually work
  if (process.env.NODE_ENV !== 'production') {
    useWebpackMiddleware(app)
  } else {
    app.use('/js', express.static(__dirname + '/public/build/js'))
  }
}

// module.exports = function(app) {
//
// };
