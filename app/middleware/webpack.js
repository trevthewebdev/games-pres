// Webpack Stuffs
import webpack from 'webpack'
import config from './../../webpack.dev.config'
import webpackDevMiddleware from 'webpack-dev-middleware'
import webpackHotMiddleware from 'webpack-hot-middleware'

const compiler = webpack(config)

const useWebpackMiddleware = (app) => {
  app.use(webpackDevMiddleware(compiler, {
    publicPath: config.output.publicPath,
    stats: { colors: true },
    hot: true
  }))

  app.use(webpackHotMiddleware(compiler, {
    log: console.log
  }))

  return app
}

export {
  useWebpackMiddleware
}
