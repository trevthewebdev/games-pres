var webpack = require('webpack');
var webpackStats = require('stats-webpack-plugin');

module.exports = {

  // the entry points to our app(s)
  entry: {
    games: ['./app/front-end/games/index.js', 'webpack/hot/dev-server', 'webpack-hot-middleware/client'],
    events: ['./app/front-end/events/index.js', 'webpack/hot/dev-server', 'webpack-hot-middleware/client'],
    players: ['./app/front-end/players/index.js', 'webpack/hot/dev-server', 'webpack-hot-middleware/client'],
  },

  // output congifuration (in memory)
  output: {
    path: '/',
    publicPath: 'http://localhost:3002/build/js',
    filename: '[name].bundle.js'
  },

  // we use es2015, we want source maps for development
  devtool: 'source-map',
  target: 'web',

  resolve: {
    extensions: ['', '.js', '.map']
  },

  module: {
    loaders: [
      {
        test: /\.js$/,
        loaders: [
          'babel-loader',
          'babel?presets[]=react,presets[]=es2015'
        ],
        exclude: /node_modules/,
      }
    ]
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpackStats('webpack.json')
  ]
}
