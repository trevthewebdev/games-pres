var path = require('path')
var webpack = require('webpack')

module.exports = {

  // the main entry of our app
  entry: {
    games: './front-end/games/entry.js',
    events: './front-end/events/entry.js',
    users: './front-end/users/entry.js',
  },

  // output congifuration
  output: {
    path: __dirname + '/public/build/js',
    filename: '[name].bundle.js'
  },

  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ['es2015', 'react']
        }
      }
    ]
  }
}
