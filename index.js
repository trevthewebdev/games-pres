import app from './app'
import config from './app/config'

// Start an express server
app.listen(config.port, function () {
  console.log('Express Server Running at localhost:' + config.port)
})
